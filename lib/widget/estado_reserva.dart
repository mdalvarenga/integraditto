import 'package:flutter/material.dart';
import 'package:integraditto/modelo/reservable.dart';
import 'package:intl/intl.dart';

class EstadoReserva extends StatelessWidget {
  final Reservable reservable;

  const EstadoReserva.para(this.reservable, {super.key});

  // Esto subirlo a algun manejador de estado o de objetos para
  // centralizar las fehcas en un solo lado
  String fechaAMostrar(DateTime fechaParaFormatear) {
    final DateFormat formatoParaMostrar = DateFormat('dd/MM/yyyy');
    return formatoParaMostrar.format(fechaParaFormatear);
  }

  bool estaReservado() => reservable.estaReservado();

  DateTime fechaReserva() => reservable.fechaReserva();

  DateTime fechaDevolucion() => reservable.fechaDevolucion();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Icon(estaReservado() ? Icons.event_busy : Icons.event_available,
                color: estaReservado() ? Colors.red : Colors.green),
            Text(estaReservado()
                ? "Reservado por ${reservable.nombreReservante()} el ${fechaAMostrar(fechaReserva())}"
                : "Disponible")
          ],
        ),
        const SizedBox(height: 10),
        if (estaReservado())
          Row(children: [
            const Icon(Icons.schedule),
            Text("Fecha de devolución: ${fechaAMostrar(fechaDevolucion())}")
          ])
      ],
    );
  }
}
