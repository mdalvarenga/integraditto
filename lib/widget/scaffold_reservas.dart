import 'package:flutter/material.dart';
import 'package:integraditto/colores_personalizados.dart';

class ScaffoldReservas extends StatefulWidget {
  Widget body;
  String titulo;

 ScaffoldReservas(this.titulo, this.body, {super.key});

  @override
  State<ScaffoldReservas> createState() => _ScaffoldReservasState();
}

class _ScaffoldReservasState extends State<ScaffoldReservas> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.titulo),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          backgroundColor: ColoresPersonalizados.fondoAzulNavbar,
          centerTitle: true,
        ),
        body: widget.body);
  }
}
