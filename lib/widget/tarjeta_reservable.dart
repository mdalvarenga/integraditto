import 'package:flutter/material.dart';
import 'package:integraditto/modelo/presentacion.dart';
import 'package:integraditto/modelo/reservable.dart';
import 'package:integraditto/pagina/detalle_reservable_page.dart';
import 'package:integraditto/widget/estado_reserva.dart';
import 'package:integraditto/widget/resumen_reservable.dart';

class TarjetaReservable extends StatelessWidget {
  final Reservable reservable;

  const TarjetaReservable(this.reservable, {super.key});

  void irAlDetalle(context, reservable) { //Esto habría que sacarlo?
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                DetalleReservablePage(Presentacion.para(reservable))));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 10,
              blurRadius: 10,
              offset: const Offset(1, 3), // Cambia la posición de la sombra
            ),
          ],
        ),
        child: GestureDetector(
            onTap: () => irAlDetalle(context, reservable),
            child: Card(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(mainAxisSize: MainAxisSize.max, children: [
                      Text(reservable.descripcion(),
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.titleMedium),
                      Row(children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              EstadoReserva.para(reservable),
                            ],
                          ),
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              IconButton(
                                  icon: const Icon(
                                    Icons.navigate_next,
                                  ),
                                  onPressed: () =>
                                      irAlDetalle(context, reservable)),
                            ])
                      ]),
                      ResumenReservable.para(reservable)
                    ])))));
  }
}
