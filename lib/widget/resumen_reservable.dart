import 'package:flutter/material.dart';
import 'package:integraditto/modelo/reservable.dart';

class ResumenReservable extends StatelessWidget {
  final Reservable reservable;

  const ResumenReservable.para(this.reservable, {super.key});

  String vecesReservado() => reservable.vecesReservado().toString();

  String creditos() => reservable.creditos().toString();

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 20,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          const Icon(Icons.library_add_check),
          Text(vecesReservado()),
          const SizedBox(width: 30),
          Icon(Icons.confirmation_number,
              color: reservable.esEspecial() ? Colors.red : Colors.lightBlue),
          Text(creditos()),
        ],
      ),
    );
  }
}
