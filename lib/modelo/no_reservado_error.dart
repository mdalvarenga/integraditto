class NoReservadoError extends UnimplementedError {
  NoReservadoError(String message) : super(message);
}