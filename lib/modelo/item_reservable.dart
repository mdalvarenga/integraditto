import 'package:intl/intl.dart';

abstract class ItemReservable {
  String descripcion();

  int creditos();

  int tiempoDeDevolucion();

  String titulo();

  String imagen();
}

class Libro extends ItemReservable {
  final String _titulo;
  final String _autor;
  final Genero _genero;
  final int _cantidadDePaginas;
  final String _urlImagen;

  Libro(this._titulo, this._autor, this._genero, this._cantidadDePaginas,
      this._urlImagen);

  @override
  String descripcion() {
    return "\"$_titulo\", $_autor";
  }

  @override
  int creditos() {
    return _genero.creditos();
  }

  @override
  int tiempoDeDevolucion() {
    const diasPorCada100Paginas = 1;
    int tiempoDevolucion =
        (_cantidadDePaginas / 100).ceil() * diasPorCada100Paginas;

    return tiempoDevolucion;
  }

  @override
  String titulo() {
    return _titulo;
  }

  @override
  String imagen() {
    return _urlImagen;
  }

  String autor() {
    return _autor;
  }

  String nombreGenero() {
    return _genero._nombre;
  }
}

class Genero {
  final String _nombre;
  final int _creditos;

  Genero(this._nombre, this._creditos);

  int creditos() {
    return _creditos;
  }
}

class Pelicula extends ItemReservable {
  final String _titulo;
  final int _duracion;
  final List<String> _actores;
  final String _urlImagen;

  Pelicula(this._titulo, this._duracion, this._actores, this._urlImagen);

  @override
  String descripcion() {
    return "\"$_titulo\", $_duracion minutos";
  }

  @override
  int creditos() {
    return 10 * _actores.length;
  }

  @override
  int tiempoDeDevolucion() {
    return _duracion >= 120 ? 5 : 3;
  }

  @override
  String titulo() {
    return _titulo;
  }

  @override
  String imagen() {
    return _urlImagen;
  }

  int duracion() {
    return _duracion;
  }

  List<String> actores() {
    return _actores;
  }
}

class Revista extends ItemReservable {
  final String _nombre;
  final int _numero;
  final DateTime _fechaDePublicacion;
  final int _cantidadLetras;
  final String _urlImagen;

  Revista(this._nombre, this._numero, this._fechaDePublicacion, this._cantidadLetras,
      this._urlImagen);

  get textoMes => DateFormat('MMMM', 'es').format(_fechaDePublicacion);

  get anio => _fechaDePublicacion.year;

  @override
  String descripcion() {
    return "\"$_nombre\", número $_numero, $textoMes $anio";
  }

  @override
  int creditos() {
    return 3 * _cantidadLetras;
  }

  @override
  int tiempoDeDevolucion() {
    final int anio = _fechaDePublicacion.year;
    return anio < 1980
        ? 2
        : anio >= 1980 && anio < 2000
            ? 3
            : 5;
  }

  @override
  String titulo() {
    return _nombre;
  }

  @override
  String imagen() {
    return _urlImagen;
  }

  int numero() {
    return _numero;
  }

  DateTime fechaPublicacion() {
    return _fechaDePublicacion;
  }

  int cantidadDePalabras() {
    return _cantidadLetras;
  }
}
