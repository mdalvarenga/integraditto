import 'package:integraditto/modelo/item_reservable.dart';
import 'package:integraditto/modelo/reservable.dart';

class ClienteReservas {
  Future<List<Reservable>> obtenerTodos() {
    const urlImagenPadrino = "https://www.race.es/revista-autoclub/wp-content/uploads/sites/4/2022/05/el-padrino-el-clasico-de-los-clasicos-759x500.jpg";
    final ahora = DateTime.now();
    final reservante = Reservante("Jose luis");
    final actores = ["Nombre de actor", "Nombre de actor 2"];
    final libro1 = Libro(
        "Un libraaaaaaaaaaa",
        "Algun autor",
        Genero("terror", 15),
        400, urlImagenPadrino);
    final libro2 =
        Libro("Otro libro", "Mismo autor", Genero("terror2", 300), 1232, urlImagenPadrino);
    final pelicula1 = Pelicula("El padrino 2", 190, actores, urlImagenPadrino);
    final pelicula2 = Pelicula("El padrino 3", 171, actores, urlImagenPadrino);
    final revista1 = Revista("Una revista", 12, ahora, 1000, urlImagenPadrino);
    final revista2 = Revista(
        "Otra revista", 87, ahora.subtract(const Duration(days: 12)), 1000, urlImagenPadrino);

    List<Reservable> reservables = [
      Reservable.noReservado(libro1, 1233),
      Reservable.reservado(libro2, 8665, reservante, ahora),
      Reservable.noReservado(pelicula1, 20400),
      Reservable.reservado(pelicula2, 15678, reservante, ahora),
      Reservable.noReservado(revista1, 65),
      Reservable.reservado(revista2, 13, reservante, ahora)
    ];
    return Future.delayed(const Duration(seconds: 2), () {
      return reservables;
    });
  }
}
