import 'package:integraditto/modelo/no_reservado_error.dart';

import 'item_reservable.dart';

class Reservable {
  ItemReservable _item;
  EstadoReservable _estado;
  final int _vecesReservado;

  Reservable._(this._item, this._estado, this._vecesReservado);

  factory Reservable.reservado(ItemReservable item, int vecesReservado,
      Reservante reservante, DateTime fechaReserva) {
    EstadoReservable estado = Reservado(reservante, fechaReserva);
    return Reservable._(item, estado, vecesReservado);
  }

  factory Reservable.noReservado(ItemReservable item, int vecesReservado) {
    return Reservable._(item, NoReservado(), vecesReservado);
  }

  int vecesReservado() {
    return _vecesReservado;
  }

  bool estaReservado() {
    return _estado.estaReservado();
  }

  Reservante reservante() {
    return _estado.quienReserva();
  }

  DateTime fechaReserva() {
    return _estado.fechaReservaActual();
  }

  DateTime fechaDevolucion() {
    return _estado
        .fechaReservaActual()
        .add(Duration(days: _item.tiempoDeDevolucion()));
  }

  String nombreReservante() {
    return _estado.nombreReservante();
  }

  bool esEspecial() {
    return _item.creditos() > 500;
  }

  String descripcion(){
    return _item.descripcion();
  }

  int creditos(){
    return _item.creditos();
  }

  int tiempoDeDevolucion(){
    return _item.tiempoDeDevolucion();
  }

  String tituloItem() {
    return _item.titulo();
  }

  String imagen() {
    return _item.imagen();
  }

  ItemReservable item() {
    return _item;
  }
}

abstract class EstadoReservable {
  bool estaReservado();

  Reservante quienReserva();

  DateTime fechaReservaActual();

  String nombreReservante();
}

class Reservado extends EstadoReservable {
  final Reservante _reservante;
  final DateTime _fechaReservaActual;

  Reservado(this._reservante, this._fechaReservaActual);

  @override
  bool estaReservado() {
    return true;
  }

  @override
  Reservante quienReserva() {
    return _reservante;
  }

  @override
  DateTime fechaReservaActual() {
    return _fechaReservaActual;
  }

  @override
  String nombreReservante() {
    return _reservante.nombre;
  }
}

class NoReservado extends EstadoReservable {
  @override
  bool estaReservado() {
    return false;
  }

  @override
  Reservante quienReserva() {
    throw NoReservadoError("No soy una reserva capo!");
  }

  @override
  DateTime fechaReservaActual() {
    throw NoReservadoError("No soy una reserva capo!");
  }

  @override
  String nombreReservante() {
    throw NoReservadoError("No soy una reserva capo!");
  }
}

class Reservante {
  final String nombre;

  Reservante(this.nombre);
}
