import 'package:integraditto/modelo/reservable.dart';

import 'item_reservable.dart';

class Presentacion {
  String _titulo;

  String get titulo => _titulo;
  String _urlImagen;
  String _primerDetalle;
  String _segundoDetalle;
  DateTime _devolucionEstimada;
  Reservable _reservable;

  Presentacion._(this._titulo, this._urlImagen, this._primerDetalle,
      this._segundoDetalle, this._devolucionEstimada, this._reservable);

  factory Presentacion.paraLibro(Reservable reservable) {
    Libro libro = reservable.item() as Libro;
    DateTime ahora = DateTime.now();
    DateTime devolucionEstimada =
        ahora.add(Duration(days: libro.tiempoDeDevolucion()));
    return Presentacion._(
        reservable.tituloItem(),
        reservable.imagen(),
        "Autor: ${libro.autor()}",
        "Género: ${libro.nombreGenero()}",
        devolucionEstimada,
        reservable);
  }

  factory Presentacion.paraRevista(Reservable reservable) {
    Revista revista = reservable.item() as Revista;
    DateTime ahora = DateTime.now();
    DateTime devolucionEstimada =
        ahora.add(Duration(days: revista.tiempoDeDevolucion()));

    return Presentacion._(
        reservable.tituloItem(),
        reservable.imagen(),
        "Número: ${revista.numero()}. Fecha de publicación: ${revista.fechaPublicacion()}",
        "Total de palabras: ${revista.cantidadDePalabras()}",
        devolucionEstimada,
        reservable);
  }

  factory Presentacion.paraPelicula(Reservable reservable) {
    Pelicula pelicula = reservable.item() as Pelicula;
    DateTime ahora = DateTime.now();
    DateTime devolucionEstimada =
        ahora.add(Duration(days: pelicula.tiempoDeDevolucion()));

    return Presentacion._(
        reservable.tituloItem(),
        reservable.imagen(),
        "Duración: ${pelicula.duracion()} minutos.",
        "Elenco: ${pelicula.actores().join(', ')}",
        devolucionEstimada,
        reservable);
  }

  factory Presentacion.para(Reservable reservable) {
    if (reservable.item() is Libro) {
      return Presentacion.paraLibro(reservable);
    } else if (reservable.item() is Pelicula) {
      return Presentacion.paraPelicula(reservable);
    } else if (reservable.item() is Revista) {
      return Presentacion.paraRevista(reservable);
    } else {
      throw ArgumentError('Tipo de item no válido.');
    }
  }

  String get urlImagen => _urlImagen;

  String get primerDetalle => _primerDetalle;

  String get segundoDetalle => _segundoDetalle;

  DateTime get devolucionEstimada => _devolucionEstimada;

  Reservable get reservable => _reservable;

  bool get estaReservado => _reservable.estaReservado();
}
