import 'package:flutter/material.dart';
import 'package:integraditto/cargando.dart';
import 'package:integraditto/modelo/cliente_reservas.dart';
import 'package:integraditto/modelo/reservable.dart';
import 'package:integraditto/widget/scaffold_reservas.dart';
import 'package:integraditto/widget/tarjeta_reservable.dart';

class ReservasPage extends StatefulWidget {
  const ReservasPage({super.key});

  @override
  State<ReservasPage> createState() => _ReservasPageState();
}

class _ReservasPageState extends State<ReservasPage> {
  @override
  Widget build(BuildContext context) {
    return ScaffoldReservas(
        'Reservaditto',
        FutureBuilder<List<Reservable>>(
            future: ClienteReservas().obtenerTodos(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text("No se pueden cargar los reservables"),
                );
              }
              if (snapshot.hasData) {
                final reservables = snapshot.data ?? [];
                return reservables.isEmpty
                    ? const Text("Vaciooooo")
                    : Center(
                        child: KeyedSubtree(
                            key: ValueKey<int>(reservables.length),
                            child: ListView.separated(
                                separatorBuilder: (_, a) =>
                                    const Divider(thickness: 2, height: 5),
                                padding: const EdgeInsets.only(top: 7.0),
                                scrollDirection: Axis.vertical,
                                itemCount: reservables.length,
                                itemBuilder: (BuildContext context,
                                        int index) =>
                                    TarjetaReservable(reservables[index]))));
              }
              return const Cargando();
            }));
  }
}
