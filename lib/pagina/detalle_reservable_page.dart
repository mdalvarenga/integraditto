import 'package:flutter/material.dart';
import 'package:integraditto/modelo/presentacion.dart';
import 'package:integraditto/widget/estado_reserva.dart';
import 'package:integraditto/widget/resumen_reservable.dart';
import 'package:integraditto/widget/scaffold_reservas.dart';

class DetalleReservablePage extends StatefulWidget {
  final Presentacion presentacion;

  const DetalleReservablePage(this.presentacion, {super.key});

  @override
  State<DetalleReservablePage> createState() => _DetalleReservablePageState();
}

class _DetalleReservablePageState extends State<DetalleReservablePage> {
  late String valorDelSelector;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Color surfaceColor = Theme.of(context).colorScheme.surface;
    return ScaffoldReservas(
        widget.presentacion.titulo,
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            color: surfaceColor,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 200,
                width: 400,
                child: Image.network(
                  widget.presentacion.urlImagen,
                  height: 150,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(height: 10),
              Text(widget.presentacion.primerDetalle,
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              Text(widget.presentacion.segundoDetalle,
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              EstadoReserva.para(widget.presentacion.reservable),
              const SizedBox(height: 10),
              ResumenReservable.para(widget.presentacion.reservable),

            ]),
          ),
        ));
  }
}
