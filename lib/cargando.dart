import 'package:flutter/material.dart';

class Cargando extends StatelessWidget {
  //Esto puede ser el Quien es ese pokemon!? como animación
  const Cargando({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
