import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:integraditto/colores_personalizados.dart';
import 'package:integraditto/pagina/reservas_page.dart';
import 'package:intl/date_symbol_data_local.dart';

Future<void> main() async {
  await initializeDateFormatting('es');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const estiloTextoNavbar = TextStyle(
        color: ColoresPersonalizados.amarilloCentral,
        fontSize: 24,
        fontWeight: FontWeight.bold);
    const negritaTamanioMedio =
        TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
    return MaterialApp(
      title: 'Reservaditto',
      themeMode: ThemeMode.light,
      theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueAccent).copyWith(surface: Colors.grey.shade200),
          useMaterial3: true,
          textTheme: TextTheme(
              titleLarge:
                  GoogleFonts.sourceSansPro(textStyle: estiloTextoNavbar),
              titleMedium:
                  GoogleFonts.sourceSansPro(textStyle: negritaTamanioMedio))),
      home: const ReservasPage(),
    );
  }
}
