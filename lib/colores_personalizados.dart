import 'package:flutter/material.dart';

class ColoresPersonalizados {
  static const Color amarilloCentral = Color.fromRGBO(255, 203, 5, 1);
  static const Color fondoAzulNavbar = Color.fromRGBO(60, 91, 167, 1);
  static const Color fondoGris = Color.fromRGBO(203, 202, 202, 1);
  static const Color fondoNaranja = Color.fromRGBO(245, 177, 99, 1);
}
