import 'package:flutter_test/flutter_test.dart';
import 'package:integraditto/modelo/item_reservable.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  setUp(() async => await initializeDateFormatting('es'));
  group("Libro", () {
    const titulo = "Un libro de terror";
    const autor = "Algun autor";
    final generoDeTerror = Genero("terror", 800);
    const cantidadDePaginas = 400;
    const imagen = "una url";
    final libroDeTerror =
        Libro(titulo, autor, generoDeTerror, cantidadDePaginas, imagen);

    test('tiene titulo y autor en la descripción', () async {
      expect(libroDeTerror.descripcion(),
          equals("\"Un libro de terror\", Algun autor"));
    });

    test('de acción tiene una cantidad de créditos', () async {
      final genero = Genero("accion", 400);
      final libroDeAccion =
          Libro(titulo, autor, genero, cantidadDePaginas, imagen);

      expect(libroDeAccion.creditos(), equals(400));
    });

    test('de terror tiene una cantidad de créditos', () async {
      expect(libroDeTerror.creditos(), equals(800));
    });

    test('tiene un tiempo de devolucion de un 1 dia por cada 100 paginas',
        () async {
      expect(libroDeTerror.tiempoDeDevolucion(), equals(4));
    });

    test(
        'tiene un tiempo de devolucion de un 1 dia por cada 100 paginas redondeado hacia arriba',
        () async {
      const paginas = 899;
      final libro = Libro(titulo, autor, generoDeTerror, paginas, imagen);
      expect(libro.tiempoDeDevolucion(), equals(9));
    });
  });

  group("Pelicula", () {
    const titulo = "El padrino 2";
    const duracion = 190;
    final actores = ["Nombre de actor", "Nombre de actor 2"];
    const imagen = "una url";
    final peliculaLarga = Pelicula(titulo, duracion, actores, imagen);

    test('tiene titulo y la duracion en la descripción', () async {
      expect(
          peliculaLarga.descripcion(), equals("\"El padrino 2\", 190 minutos"));
    });

    test(
        'tiene una cantidad de creditos basada en 10 veces la cantidad de actores',
        () async {
      expect(peliculaLarga.creditos(), equals(20));
    });

    test('si dura 2 horas o mas se devuelve en 5 dias', () async {
      expect(peliculaLarga.tiempoDeDevolucion(), equals(5));
    });

    test('si dura menos de 2 horas o mas se devuelve en 3 dias', () async {
      const titulo0 = "El padrino 4";
      const duracion0 = 110;
      final actores = ["Nombre de actor", "Nombre de actor 2"];
      final peliculaCorta = Pelicula(titulo0, duracion0, actores, imagen);
      expect(peliculaCorta.tiempoDeDevolucion(), equals(3));
    });
  });

  group("Revista", () {
    const nombre = "Una revista";
    const numero = 81;
    const cantidadLetras = 369;
    DateTime fechaPublicacion = DateTime(1980, 8, 10);
    const imagen = "una url";
    final revistaDel80 =
        Revista(nombre, numero, fechaPublicacion, cantidadLetras, imagen);

    test('tiene nombre, numero y fecha de salida en la descripción', () async {
      expect(revistaDel80.descripcion(),
          equals("\"Una revista\", número 81, agosto 1980"));
    });

    test(
        'tiene una cantidad de creditos basada en 3 veces la cantidad de letras de su publicacion',
        () async {
      expect(revistaDel80.creditos(), equals(1107));
    });

    test('de 1980 o anterior, se devuelve en 2 dias', () async {
      const nombre0 = "Una revista";
      const numero0 = 81;
      const cantidadLetras0 = 369;
      DateTime fechaPublicacion0 = DateTime(1970, 8, 10);
      final revistaPreviaAl80 =
          Revista(nombre0, numero0, fechaPublicacion0, cantidadLetras0, imagen);
      expect(revistaPreviaAl80.tiempoDeDevolucion(), equals(2));
    });

    test('entre a 1981 y 1999, se devuelve en 3 dias', () async {
      const nombre0 = "Una revista";
      const numero0 = 81;
      const cantidadLetras1 = 369;
      DateTime fechaPublicacion0 = DateTime(1990, 8, 10);
      final revistaDeLos90 =
          Revista(nombre0, numero0, fechaPublicacion0, cantidadLetras1, imagen);
      expect(revistaDeLos90.tiempoDeDevolucion(), equals(3));
    });

    test('del 2000 en adelante, se devuelve en 5 dias', () async {
      const nombre0 = "Una revista";
      const numero0 = 81;
      const cantidadLetras1 = 369;
      DateTime fechaPublicacion0 = DateTime(2013, 8, 10);
      final revistaPost2000 =
          Revista(nombre0, numero0, fechaPublicacion0, cantidadLetras1, imagen);
      expect(revistaPost2000.tiempoDeDevolucion(), equals(5));
    });
  });
}
