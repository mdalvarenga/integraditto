import 'package:flutter_test/flutter_test.dart';
import 'package:integraditto/modelo/item_reservable.dart';
import 'package:integraditto/modelo/no_reservado_error.dart';
import 'package:integraditto/modelo/reservable.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  setUp(() async => await initializeDateFormatting('es'));
  group("Reservable", () {
    const nombre = "Una revista";
    const numero = 81;
    const cantidadLetras = 369;
    DateTime fechaPublicacion = DateTime(1980, 8, 10);
    const imagen = "una url";
    final revistaEspecial =
        Revista(nombre, numero, fechaPublicacion, cantidadLetras, imagen);

    group("Reservado", () {
      const vecesReservado = 10;
      final reservante = Reservante("reservante");
      final fechaReserva = DateTime(2023, 8, 10);
      final reservableReservado = Reservable.reservado(
          revistaEspecial, vecesReservado, reservante, fechaReserva);
      test('fue reservado una cantidad de veces', () async {
        expect(reservableReservado.vecesReservado(), equals(10));
      });

      test('Reservado, está reservado', () async {
        expect(reservableReservado.estaReservado(), true);
      });

      test('tiene la descripcion del item', () async {
        expect(
            reservableReservado.descripcion(), equals("\"Una revista\", número 81, agosto 1980"));
      });

      test('tiene un reservante', () async {
        expect(reservableReservado.reservante(), equals(reservante));
      });

      test('tiene una fecha de reserva', () async {
        expect(reservableReservado.fechaReserva(), equals(fechaReserva));
      });

      test(
          'tiene una fecha de devolución en función del item y la fecha de reserva actual',
          () async {
        expect(reservableReservado.fechaDevolucion(),
            equals(fechaReserva.add(const Duration(days: 3))));
      });

      test('tiene un nombre reservante', () async {
        expect(reservableReservado.nombreReservante(), equals("reservante"));
      });

      test('es especial si tiene mas de 500 creditos', () async {
        expect(reservableReservado.esEspecial(), equals(true));
      });
    });

    group("No Reservado", () {
      const vecesReservado = 10;
      final reservableNoReservado =
          Reservable.noReservado(revistaEspecial, vecesReservado);
      test('fue reservado una cantidad de veces', () async {
        expect(reservableNoReservado.vecesReservado(), equals(10));
      });

      test('NoReservado, no está reservado', () async {
        expect(reservableNoReservado.estaReservado(), false);
      });

      test('tiene la descripcion del item', () async {
        expect(reservableNoReservado.descripcion(),
            equals("\"Una revista\", número 81, agosto 1980"));
      });

      test('lanza un error al pedir un reservante', () async {
        expect(() => reservableNoReservado.reservante(),
            throwsA(isA<NoReservadoError>()));
      });

      test('tiene una fecha de reserva', () async {
        expect(() => reservableNoReservado.fechaReserva(),
            throwsA(isA<NoReservadoError>()));
      });

      test(
          'tiene una fecha de devolución en función del item y la fecha de reserva actual',
          () async {
        expect(() => reservableNoReservado.fechaDevolucion(),
            throwsA(isA<NoReservadoError>()));
      });

      test('tiene un nombre reservante', () async {
        expect(() => reservableNoReservado.nombreReservante(),
            throwsA(isA<NoReservadoError>()));
      });

      test('es especial si tiene mas de 500 creditos', () async {
        expect(reservableNoReservado.esEspecial(), equals(true));
      });
    });
  });
}
